package com.s53.demo.service;

import com.s53.demo.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    void getAll() {
        Page<User> page = userService.getAll(PageRequest.of(0, 2));
        outPage(page);
        System.out.println("===========================");
        page = userService.getAll(PageRequest.of(2, 2));
        outPage(page);
    }

    @Test
    void getAllPage() {
        User user = new User();
        user.setAccount("admin");
        user.setRealName("你");
        user.setSex(1);
        Page<User> page = userService.getAll(user, PageRequest.of(0, 2));
        outPage(page);
        System.out.println("===========================");
        page = userService.getAll(user, PageRequest.of(1, 2));
        outPage(page);
    }

    private void outPage(Page<User> page) {
        System.out.println("总记录数：" + page.getTotalElements());
        System.out.println("总页数：" + page.getTotalPages());
        System.out.println("当前页：" + page.getNumber());
        System.out.println("每页条数：" + page.getSize());
        System.out.println("分页结果：" + page.getContent());
    }
}