package com.s53.demo.service;

import com.s53.demo.dao.UserDao;
import com.s53.demo.pojo.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {
    @Resource
    private UserDao userDao;

    public boolean add(User user) {
        userDao.save(user);
        return true;
    }

    public boolean delete(Long id) {
        userDao.deleteById(id);
        return true;
    }

    public boolean update(User user) {
        userDao.save(user);
        return true;
    }

    public User get(Long id) {
        return userDao.getOne(id);
    }

    public List<User> getAll() {
        return userDao.findAll();
    }

    public Page<User> getAll(Pageable pageable) {
        return userDao.findAll(pageable);
    }

    public void test() {
        System.out.println("tfest");
        this.get(1L);
        System.out.println("test");
        System.out.println("te1st");
        System.out.println("tes2t");
    }

    public Page<User> getAll(User user, Pageable pageable) {
        System.out.println();
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("realName", ExampleMatcher.GenericPropertyMatchers.contains())
                .withIgnorePaths("account");
        System.out.println("matcher = " + matcher);
        return userDao.findAll(Example.of(user, matcher), pageable);
    }
}
